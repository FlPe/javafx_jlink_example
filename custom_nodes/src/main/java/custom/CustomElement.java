package custom;

import javafx.beans.NamedArg;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class CustomElement extends VBox {
    @FXML
    private Label myLabel;   // Label with id "myLabel" in the fxml file

    public CustomElement(){
        super();

        FXMLLoader loader = new FXMLLoader(CustomElement.class.getResource("/custom/custom.fxml"));
        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        sceneProperty().addListener((observable, oldValue, newValue) -> {
            if(oldValue == null && newValue != null){
                // Do something when the element is inside a scene (and length of elements are calculated)
            }
        });
    }

    // If an attribute in used with the element
    public CustomElement(@NamedArg("itemName") String itemName){
        this();
        myLabel.setText(itemName);
    }
}