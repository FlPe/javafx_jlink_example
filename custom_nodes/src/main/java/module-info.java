module javafx.jlink.example.custom_nodes {
    // requires JavaFX
    requires javafx.controls;
    requires javafx.fxml;

    // Open the package containing custom elements
    // "opens" is required by JavaFX because of the usage of "@FXML" in code
    // JavaFX should be able to access private fields
    opens custom;
}