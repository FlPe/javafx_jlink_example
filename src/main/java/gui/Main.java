package gui;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("My window");
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/window.fxml"));
        primaryStage.setScene(new Scene(root, 500, 600));
        primaryStage.show();
    }
}